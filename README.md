# Qt-Server-Sent-Events-Demo

A Qt5 Server Sent Events (SSE) demo.

## Build status

[![pipeline status](https://gitlab.com/dylanvanassche/Qt-Server-Sent-Events-Demo/badges/master/pipeline.svg)](https://gitlab.com/dylanvanassche/Qt-Server-Sent-Events-Demo/commits/master)


## Build instructions

- `cmake CMakeLists.txt`
- `make`
- `./qt-sse-demo`
